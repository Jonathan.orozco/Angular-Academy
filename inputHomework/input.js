(function(){
    var scope = {
     container : document.querySelector('[data-module="input"]'),
     init(){
        this.container.innerHTML=this.template;
        scope.render()
     },
    render(){
        this.mount();
    },
    mount(){
      // var fooArray= Object.keys(this.data);
    // for(const element in this.data){
    //     var htmlTag= this.container.querySelectorAll('[data-value="'+element+'"]');
    //     if(htmlTag){
    //         htmlTag.forEach(key =>{
    //             key.innerHTML = this.data[element];
    //         });
    //     }
    // }
    
    var htmlClickTag = this.container.querySelectorAll('[data-event-keyboard=keyPress]');
    
    if(htmlClickTag){
    
    htmlClickTag.forEach(key =>{
    
                 key.addEventListener('keyup',function(){
    
                 document.querySelector('[data-value="text"]').innerHTML
    
                 = document.querySelector('[data-event-keyboard=keyPress]').value;
                 })
        })
}  
    },
    //scope.container.querySelector('[data-event-click="startClock"]')
            // fooArray.forEach(element => {
            //     document.querySelector('[data-value="'+element+'"]').innerHTML=scope.data[element];
            // });
  
     template:` 
    <article class="input">
    <section class="controls">
    <input data-event-keyboard="keyPress"></input>
    
</section>
    <section class="display">
        <span class="input" data-value="text"></span>
    </section>
    
    </article>`,
    
    data:{seconds:0,minutes:0,hours:0},
    methods:{
        updateClock(){
            if(scope.data.seconds==60)
            {
                scope.data.seconds=0;
                scope.data.minutes ++;
            }
            if(scope.data.minutes==60)
            {
                scope.data.minutes=0;
                scope.data.hours ++;
            }
           
        },
        start(){
        if(!scope.data.clockId)
        {
            scope.data.clockId= setInterval(function(){
                scope.data.seconds ++;
                scope.methods.updateClock();
                scope.render();
                   
            },1000)
        }
        },
        pauseClock(){
            clearInterval(scope.data.clockId)
            scope.data.clockId=0;
        },
        stopClock(){
            clearInterval(scope.data.clockId)
            scope.data.seconds =0;
            scope.data.minutes =0;
            scope.data.hours =0;
            scope.data.clockId=0;
            scope.render();
        }
    }
    }
    scope.init();
    })();
    
    
     
    
    