import Vue from 'vue'
import App from './App.vue'
import Router from 'vue-router'
import Main from './Main.vue'
import someText from './someText.vue'
import Dinamic from './Dinamic.vue'

Vue.use(Router)


var router = new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main
    },
    {
      path: '/someText',
      name: 'someText',
      component: someText
    },
    {
      path: '/dinamic/:id',
      name: 'dinamic',
      component: Dinamic
    }
  ]
})

new Vue({
  el: '#app',
  router,
  render: h => h(App),
})

