'use strict';

angular.module('myApp', [

])

.filter('doubleCero', function(){
    return function(value){;
        return('00' + value).slice(-2);
    }
})

.directive('clockDirective',function(){
    return{
        controller:"clockCtrl",
        controllerAs:"ctrl",
        restrict:"E",
        templateUrl:"clock.html"
    }
})

.controller('clockCtrl',['$interval',function($interval) {
    this.seconds= 0;
    this.minutes= 0;
    this.hours= 0;

    this.startClock =function(){
          if (!this.clockId) {
              this.clockId = $interval(function () {
                  this.updateClock();
              }.bind(this), 100);
          }
        }


    this.updateClock =function(){
        if (this.seconds < 59) {
            this.seconds++;
        } else {
            this.seconds = 0;
            this.minutes++;
        }
        if (this.minutes >= 60) {
            this.minutes = 0;
            this.hours++;
        }
    }

    this.pauseClock =function(){
            $interval.cancel(this.clockId);
            this.clockId = 0;
        }
    
        this.stopClock =function(){
            $interval.cancel(this.clockId)
            this.seconds = 0;
            this.minutes = 0;
            this.hours = 0;
            this.clockId = 0;
        }

}]);

