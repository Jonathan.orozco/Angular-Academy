export interface People {
    name;
    hight;
    mass;
    hair_color;
    skin_color;
    eye_color;
    birth_year;
    gender;
    homeworld;
}
