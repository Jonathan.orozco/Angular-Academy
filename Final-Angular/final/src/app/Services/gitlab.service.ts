import {Injectable} from '@angular/core';
import {Http,Headers,Response, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import "rxjs/add/operator/map";
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {People} from '../Models/people';

@Injectable(
)
export class gitlabService {
   private _servicelUrl = 'https://swapi.co/api/';
   private _getByQuarterUrl="";
   private _getRolesByUserUrl="";
   private _getPeopleUrl = "";
   
    constructor(private _http: Http) {}

    getDataByQuarter( quarter: number, year: number):Observable <People>
    {
       
        this._getByQuarterUrl="ExecDashBoard/?quarter=" + quarter +"&year=" + year;
             
       return this._http.get(this._servicelUrl+this._getByQuarterUrl).map((response:Response)=> 
                       <People>response.json()||{})
                       .do(data => console.log("All: " + JSON.stringify(data)))
                       .catch(this.handleError);
    }

    postExecDash(execDash: People){
       let headers = new Headers({ 'Content-Type': 'application/json' });
       let options = new RequestOptions({ headers: headers });
       this._getByQuarterUrl="ExecDashBoard/";

       return this._http.post(this._servicelUrl+this._getByQuarterUrl, execDash, options);
                       //  .map()
    }

    getPeople(){
       {
           this._getPeopleUrl="people";

           return this._http.get(this._servicelUrl+this._getRolesByUserUrl).map((response:Response)=>
               <People>response.json() || {})            
               .do(response => console.log("All: " + JSON.stringify(response)))
               .catch(this.handleError);
       }

    }

    private handleError(error:Response){
        console.error(error);
        return Observable.throw(error.json() || 'Ser error');
    }
   // getAllQuarters(){
   //     return this._http.get(this._fuelUrl+"ExecDashBoard").map(res=>res.json());
                  
   
   // }
}
