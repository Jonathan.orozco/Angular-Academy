(function(angular){
    'use strict';

    angular
        .module('myApp.directives')
        .directive('clockDirective',Definition);

        function Definition(){    
        return{
                controller:"clockCtrl",
                controllerAs:"ctrl",
                restrict:"E",
                templateUrl:"templates/clock.html"
            }
        }
})(angular)