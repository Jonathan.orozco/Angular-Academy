(function(angular){
    'use strict';

    angular
        .module('myApp.directives')
        .directive('movieDirective',Definition);

        function Definition(){    
        return{
                controller:"clockCtrl",
                controllerAs:"ctrl",
                restrict:"E",
                templateUrl:"templates/movie.html"
            }
        }
})(angular)