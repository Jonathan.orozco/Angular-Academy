(function(angular){
    'use strict';

    angular
        .module('myApp.directives')
        .directive('loginDirective',Definition);

        function Definition(){    
        return{
                controller:"loginCtrl",
                controllerAs:"ctrl",
                restrict:"E",
                templateUrl:"templates/login.html"
            }
        }
})(angular)