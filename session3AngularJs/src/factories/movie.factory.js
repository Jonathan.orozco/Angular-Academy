(function(angular){
    'use strict';

    angular
        .module('myApp.factories')
        .factory('movieFactory', function($q, $http){
            var apiKey='31690548';
            return{
                getPage: function(idx, term){
                    idx = idx || 1;
                    return $q(function(resolve){
                        $http
                            .get(`https:www.omdbapi.com/?apiKey=${apiKey}&s=${term}`)
                            .then(function(response){
                                resolve(response.data.Search.map(function(item){
                                    return{
                                        title: item.Title,
                                        year: item.Year,
                                        poster: item.Poster
                                    }
                                }))
                            })
                    })
                }
            }
        })
        
})(angular)