(function(angular){
    'use strict';
   
angular
    .module('myApp.filters')
    .filter('doubleCero',Definition);
    
   function Definition(){
    return function(value){
        return('00' + value).slice(-2);
       }
    }
})(angular)