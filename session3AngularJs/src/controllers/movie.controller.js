(function (angular) {
    'use strict';

    angular
        .module('myApp.controllers')
        .controller('movieCtrl', Definition);

    Definition.$inject = [
        'movieFactory','$rootScope'
    ];

    function Definition(movieFactory,$rootScope) {

        var vm = this;
        vm.value=false;
               
        
        var Search= function(text){
            movieFactory.getPage(1,text).then(
                function(movies){
                vm.movies=movies;
                }
            ) 
        } 
        
        $rootScope.$on('OnUpdate', function(event, args) {
            vm.value = args.value;
          });

        vm.search= Search; 
       
    }
})(angular)