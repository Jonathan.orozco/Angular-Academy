(function (angular) {
    'use strict';

    angular
        .module('myApp.controllers')
        .controller('clockCtrl', Definition);

    Definition.$inject = [
        '$interval'
    ];

    function Definition($interval) {

        var vm = this;

        vm.hours = 0;
        vm.minutes = 0;
        vm.seconds = 0;
        vm.interval = null;
        vm.status = null;

       


     var   Start = function () {
            if (!vm.clockId) {
                vm.clockId = $interval(function () {
                    vm.update();
                }.bind(vm), 100);
            }
        }


       var Update = function () {
            if (vm.seconds < 59) {
                vm.seconds++;
            } else {
                vm.seconds = 0;
                vm.minutes++;
            }
            if (vm.minutes >= 60) {
                vm.minutes = 0;
                vm.hours++;
            }
        }

        var Pause = function () {
            $interval.cancel(vm.clockId);
            vm.clockId = 0;
        }

        var Stop = function () {
            $interval.cancel(vm.clockId)
            vm.seconds = 0;
            vm.minutes = 0;
            vm.hours = 0;
            vm.clockId = 0;
        }
        vm.start = Start;
        vm.pause = Pause;
        vm.stop = Stop;
        vm.update = Update;
    }
})(angular)