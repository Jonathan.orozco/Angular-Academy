(function (angular) {
    'use strict';

    angular
        .module('myApp.controllers')
        .controller('loginCtrl', Definition);

    Definition.$inject = [
        'loginFactory','$rootScope'
    ];

    function Definition(loginFactory,$rootScope) {

        var vm = this;


        var Login = function (text) {
           var vmValue=loginFactory.loginUser(text);
           $rootScope.$broadcast('OnUpdate',{
               
               vmValue})
           
            };

        

        vm.login = Login;

    }
})(angular)