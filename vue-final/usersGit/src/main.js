import Vue from 'vue'
import store from './store'
import App from './App'
import User from './components/User.vue'
import Info from './components/Info.vue'
import Repository from './components/Repository.vue'
import Followers from './components/Followers.vue'

import Router from 'vue-router'
import Search from './components/Search.vue'

Vue.use(Router)


var router = new Router({
  routes: [
    {
      path: '/',
      name: 'Search',
      component: Search
    },
    {
      path: '/user/:username',
      name: 'User',
      component: User,
      children: [
        {
          path: '/user/:username/Info',
          name: 'Info',
          component: Info
        },
        {
          path: '/user/:username/Repository',
          name: 'Repository',
          component: Repository
        },
        {
          path: '/user/:username/Followers',
          name: 'Followers',
          component: Followers
        }
      ]
    }
  ]
})

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
