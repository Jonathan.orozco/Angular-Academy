import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuex)
Vue.use(VueAxios, axios)


const state = {
  users: [],
  userFollowers: [],
  userRepositories:[],
  userProfile:{}
}

const mutations = {

  getUsers(state, finduser) {
    // console.log(finduser)
    Vue.axios.get(`https://api.github.com/search/users?q=${finduser}&page=1&per_page=10`).then((response) => {
      // console.log(response.data)
        { state.users = response.data.items }
      // (err) => {
      //   console.log(err)}
    })
  },
  getUser(state, userDet) {
    console.log(userDet)
    Vue.axios.get(`https://api.github.com/users/${userDet}`).then((response) => {
      // console.log(response.data)
        { state.userProfile = response.data }
        console.log(state.userProfile)
      // (err) => {
      //   console.log(err)}
    })
  },
  getFollowers(state, userFollowed) {
    console.log(userFollowed)
    Vue.axios.get(`https://api.github.com/users/${userFollowed}/followers`).then((response) => {
      // console.log(response.data)
        { state.userFollowers = response.data }
      
      // (err) => {
      //   console.log(err)}
    })
  },
  getRepositories(state, userRepository) {
    console.log(userRepository)
    Vue.axios.get(`https://api.github.com/users/${userRepository}/repos`).then((response) => {
      // console.log(response.data)
        { state.userRepositories = response.data }
        console.log(response.data)
        
      // (err) => {
      //   console.log(err)}
    })
  }

}
const getters = {
  textStatus: state => state.textUser
}
const actions = {
  getUsers({ commit, state }, finduser) {
    // console.log(finduser)
    commit('getUsers', finduser);
  },
  getUser({ commit, state }, userDet) {
    
    commit('getUser', userDet);
  },
  getFollowers({ commit, state }, userFollowed) {
    
    commit('getFollowers', userFollowed);
  },
  getRepositories({ commit, state }, userRepository) {
    console.log(userRepository)
    commit('getRepositories', userRepository);
  }

}

export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters
})
