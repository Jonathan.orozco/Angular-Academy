import { Component} from '@angular/core';
import {Album} from './models/album';

@Component({
  selector: 'app-root2',
  templateUrl: './container.component.html',
  styleUrls: ['./musicSearch.css']
})

export class Container{
  foundAlbums:Album[];
  
  albumsFunction(foundAlbums){
    this.foundAlbums=foundAlbums;
    console.log(foundAlbums);
  }
}