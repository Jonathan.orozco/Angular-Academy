import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { Container } from './container.component';
import { Search } from './search.component';
import { Display } from './display.component';



@NgModule({
  declarations: [
    AppComponent,
    Container,
    Search,
    Display
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [Container]
})
export class AppModule { }
