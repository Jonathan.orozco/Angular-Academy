import { Component, Input } from '@angular/core';
import {Album} from './models/album';

@Component({
  selector: 'display-music',
  templateUrl: './display.component.html',
  styleUrls: ['./musicSearch.css']
})

export class Display {
    albums:Album[];
    @Input() displayAlbums:Album[];
}