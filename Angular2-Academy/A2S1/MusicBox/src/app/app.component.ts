import { Component } from '@angular/core';
import {Album} from './models/album';

@Component({
  selector: 'app-root2',
  templateUrl: './musicSearch.html',
  styleUrls: ['./musicSearch.css']
})
export class AppComponent {
  title = 'app';

  text:string;
  albums:Album[];
  chkbx1=false;
  chkbx2=false;

  constructor(){
    this.albums = [
      {
        group:'uno',
        country:'USA',
        genre:'Rock',
        year:'1983'
      },
      {
        group:'dos',
        country:'USA',
        genre:'Metal',
        year:'1989'
      },
      {
        group:'uno',
        country:'USA',
        genre:'Clasic Rock',
        year:'1973'
      },
    ]
  }
  
  foundAlbums:Album[];

  Search(){
    this.foundAlbums=new Array();
    this.albums.forEach(element => {
      if(this.chkbx1==true)
      {  
       if(this.text==element.group)
            {
               this.foundAlbums.push(element);
            }
      }
      if(this.chkbx2==true)
      {
        if(this.text==element.genre)
        {
           this.foundAlbums.push(element);
        }
      }
    });
  }


}
