class Auto{
    seguro:string;
    anio:number|string;
    constructor(seguro:string, anio:string){
        this.seguro=seguro;
        this.anio=anio;
    }
}

function Estereo(argument){
    argument.estereo=true;
}

@Estereo
class Ferrari extends Auto{
     constructor(seguro:string, anio:string){
        super(seguro,anio)
        console.log(this);
    }
}