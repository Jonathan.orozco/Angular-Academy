class Module {
    constructor(structure = {
        selector: '',
        components: []
    }) {
        try {
            this.selector = structure.selector; 
            this.components = structure.components;
            this.container = document.querySelector('[data-module="'+this.selector+'"]');
            this.mount();
            } catch (e) {
            console.log(e)
        }
           
    }
    mount(){
        var indexElem = this.container.querySelectorAll(`[data-component]`);
        if(indexElem)
        {
            indexElem.forEach(element => {
                if(this.components.includes(element.getAttribute(`data-component`)))
                {
                    
                    var componente=element.getAttribute(`data-component`);
                    componente = componente.charAt(0).toLocaleUpperCase()+componente.slice(1);
                    var ins=new window[componente](element);
                }
            });
        }
    }
}