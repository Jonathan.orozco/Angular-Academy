class Component {
    constructor(structure = { 
        nodo: '',         
        template: '',
        data: {},
        methods: {}
    }) {
        try {
           // this.selector = `[data-component="${structure.selector}"]`;  //asigna los valores enviados como parametros
            this.container = structure.nodo //document.querySelector(this.selector);
            this.template = structure.template;
            this.methods = structure.methods;
            this.data = this.buildData(structure.data);  //asigna el data llamando la funcion buildData
            this.mount();                                // Llamamos al mount que hará el primer render
        } catch (e) {
            console.log(e)
        }
    }
    buildData(data = {}) {
        var computedInitalData = {}     
        this._data = data;           //creamos un data local el cual no puede ser modificado, en caso de ser modificado traeria problemas a la aplicación
        for (const propertyName in data) {  //iteramos sobre las propiedades enviadas
            if (data.hasOwnProperty(propertyName)) {  //Verificamos si el elemento tiene porpiedad, para evitar estar iterando en las propiedades del padre
                Object.defineProperty(computedInitalData, propertyName, {  //Definimos y asignamos nuevas propiedades en forma de Set y Get con los valores enviados como parametros en el data
                    set: function (x) {
                        this._data[propertyName] = x;
                        var toChange = this.container.querySelectorAll(`[data-value="${propertyName}"]`);
                        if (toChange) {
                            this.render(toChange, data[propertyName])
                        }
                    }.bind(this),//utilizamos bind para conservar el contexto 
                    get: () => (this._data[propertyName])
                });
            }
        }
        return computedInitalData;
    }
    render(element = document.querySelector(this.selector), content = this.template) { //Renderea el template enviado en caso de enviar nulo renderea este template por default
        if (Node.prototype.isPrototypeOf(element)) {
            if (element.tagName == "INPUT") {
                element.value = content;
            } else {
                element.innerHTML = content;
            }

        }
        if (NodeList.prototype.isPrototypeOf(element)) {
            element.forEach((child) => {
                this.render(child, content)
            });
        }
    }
    mount() {
        this.render(this.container, this.template); //hace el primer Render
        for (const key in this._data) {        //Busca los elementos pasados en data con _data porque _data si es iterable y data no
            if (this.data.hasOwnProperty(key)) { //verifica si tiene su propiedad para evitar iterar en la propiedad del padre
                var toChange = this.container.querySelectorAll(`[data-value="${key}"]`); //Crea un arreglo con todos los elementos en el template que coincidan con el elemento reemplazandocon  el iterado en .data en turno
                if (toChange) {
                    this.render(toChange, this.data[key]);// si hay coincidencia hace el render
                    toChange.forEach(element => {   //Itera sobre el arreglo creado anteriormente
                        if (element.tagName == 'INPUT') { //si encuetra un elemento del tipo input entonces agrega un Listener de evento input
                                element.addEventListener('input', event=> {// y asigna el valor al elemento de data 
                                this.data[key] = event.target.value;
                                })
                            if (element.getAttribute('type') == 'checkbox') {//Si elemento es un checkbox enonces se agrega un Listener de evento click
                                    element.addEventListener('click', (click) => {//Y se agrega el valor del evento
                                        this.data[key] = click.target.checked
                                        })
                                    }
                        }
                    });
                }
            }
        }
        //Agrega los metodos que serán utilizados por las clases
        const events = ['click']   
        const DOMElements = events.map(event => { //hace un mapeo entre los metodos y los elementos del template correspondientes.
            return {
                event: event,
                targets: this.container.querySelectorAll(`[data-event-${event}]`)
            };
        });
        for (const method in this.methods) {
            if (this.methods.hasOwnProperty(method)) {//si el metodo tiene propiedad entonces conserva el contexto que se tiene en ese momento con bind(this)
                this.methods[method] = this.methods[method].bind(this);
            }
        }
        DOMElements.forEach(element => {//agrega los listeners correspondientes a los elementos elementos del template donde se disparan eventos.
            element.targets.forEach(target => {
                target.addEventListener(element.event,this.methods[target.getAttribute('data-event-' + element.event)]);
            });
        });
    }
}