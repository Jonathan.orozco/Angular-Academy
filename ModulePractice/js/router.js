var Router = class Router {
    constructor(routes = {}) {
        try {
            this.routes = routes;
            this.container = document.querySelector('[data-router]');
            this.mount();
        } catch (e) {
            console.log(e)
        }
    }
    mount() {
        addEventListener('hashchange', () => {
            this.hasChange(window.location.hash);
        })
    }

    hasChange(hash) {
        hash = hash.slice(1);
        this.container.innerHTML = '';
        if (this.routes.hasOwnProperty(hash)) {
            this.container.innerHTML = '<section class="app" data-module= "app">' + this.routes[hash].template + '</div>'
            var app = new Module({ selector: 'app', components: this.routes[hash].components });
        }
        else {
            this.container.innerHTML = '<h1>Error 404 page not found</h1>'
        }

    }
}