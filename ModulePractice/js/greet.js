var Greet =class Greet extends Component {
    constructor(selectedNode) {
        super({
            nodo:selectedNode ,
            template: `
                    <article class="greet">
                    <form>
                        <label for="name">Name</label>
                        <input type="text" id="name" data-value="name">
                        <label for="lastname">Last Name</label>
                        <input type="text" id="lastname" data-value="lastname">
                    <p>
                        <span data-value="greet"></span>
                        <span data-value="name"></span>
                        <span data-value="lastname"></span>
                        <input type="checkbox" data-event-click="flagChange" data-value="flag">
                       
                    </p>
                    </form>
                    </article>`,
            data: {name:"",lastname:"",flag:"",greet:""},
            methods: {
                flagChange(){
                    if(this.data.flag)
                    {
                        this.data.greet="Hello";
                    }
                    else{
                        this.data.greet="Bye";
                    }
                }
            }
        });
    }
};
