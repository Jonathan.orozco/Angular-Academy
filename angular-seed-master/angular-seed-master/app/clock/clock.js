'use strict';

angular.module('myApp.clock', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/clock', {
    templateUrl: 'clock/clock.html',
    controller: 'clockCtrl',
    controllerAs:'ctrl'
  });
}])

.controller('clockCtrl',[function() {
    this.seconds= 0;
    this.minutes= 0;
    this.hours= 0;

    this.startClock =function(){
          if (!this.clockId) {
              this.clockId = setInterval(function () {
                  this.updateClock();
              }.bind(this), 100);
          }
        }


    this.updateClock =function(){
        if (this.seconds < 59) {
            this.seconds++;
        } else {
            this.seconds = 0;
            this.minutes++;
        }
        if (this.minutes >= 60) {
            this.minutes = 0;
            this.hours++;
        }
    }

    // startClock() =function(){
    //     if (!this.clockId) {
    //         this.clockId = setInterval(function () {
    //             this.methods.updateClock();
    //         }.bind(this), 100);
    //     }

    // }
    // pauseClock() =function(){
    //     clearInterval(this.clockId)
    //     this.clockId = 0;
    // }

    // stopClock() =function(){
    //     clearInterval(this.clockId)
    //     this.seconds = 0;
    //     this.minutes = 0;
    //     this.hours = 0;
    //     this.clockId = 0;
    // }
}]);