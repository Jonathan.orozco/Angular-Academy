var visibilityButton = document.getElementById('toggleVisibility');
var sizeButton = document.getElementById('toggleSize');
var positionButton = document.getElementById('togglePosition');
var rotationButton = document.getElementById('toggleRotation');
var windowBox = document.querySelector('.window');

visibilityButton.addEventListener('click', function() {
  windowBox.classList.toggle('hidden');
  visibilityButton.innerText = windowBox.classList.contains('hidden') ? 'Show Window' : 'Hide Window';
});

sizeButton.addEventListener('click', function() {
  windowBox.classList.toggle('big');
  sizeButton.innerText = windowBox.classList.contains('big') ? 'Make big' : 'Make small';
});

positionButton.addEventListener('click', function() {
  windowBox.classList.toggle('moved');
});

rotationButton.addEventListener('click', function() {
  windowBox.classList.toggle('rotated');
});

///////////////////////////////////////////

var animationButton = document.getElementById('toggleAnimation');
var animatedBox = document.querySelector('.animatedBox');

animationButton.addEventListener('click', function() {
  animatedBox.classList.toggle('started');
  animationButton.innerText = animatedBox.classList.contains('started') ? 'Stop' : 'Start';
});