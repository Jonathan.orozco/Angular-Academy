class Component{
    constructor(structure = {
        selector:'',
        template:'',
        data: {},
        methods:{}
    }) {
        try{
            this.selector = structure.selector;
            this.container = document.querySelector(structure.selector);
            this.template = structure.template;
            this.methods = structure.methods;
            this.data = this.buildData(structure.data);
            this.mount();
        }catch(e){
            console.log(e)
        }
    }
    buildData(data={}){
        var computedInitialData = {};
        this._data = data;
        for(const key in data){
            if(data.hasOwnProperty(key)){
                Object.defineProperty(computedInitialData, key, {
                set: function(x){
                    this._data[key]=x;
                    var toChange = this.container.querySelectorAll(`[data-value=" ${key} "]`);
                    if(toChange){
                        this.render(toChange, data[key])
                    };
                }.bind(this),
                get: () =>(this._data[key])
            });
          }
        }
        return computedInitialData;
            }
    render(element= document.querySelector(this.selector),content=this.template){
        if(Node.prototype.isPrototypeOf(element)){
            if(element.tagName=="INPUT"){
            element.value=content;
        }else{
            element.innerHTML=content;
        }
    }
        if(NodeList.prototype.isPrototypeOf(element)){
            element.forEach((child)=>{
                this.render(child, content)
            });
        }
            }
    mount(){
        this.render(this.container, this.template);
        //Adding Data
        for(const key in this._data){
            if(this._data.hasOwnProperty(key)){
                var toChange= this.container.querySelectorAll(`[data-value=" ${key} "]`);
                if(toChange){
                    toChange.forEach(element =>{
                        this.render(element, this.data[key]);
                        //If the element is an input Listener
                        if(element.tagName== 'INPUT'){
                            element.value = this.data[key];
      
                        element.addEventListener('input',function(event){
                            this.data[key]= event.target.value;
                        }.bind(this))
                    if(element.getAttribute('type')=='checkbox'){
                        element.addEventListener('click',function(click){
                            if(click.target.getAttribute('type')=="checkbox"){
                                  this.data[key]=click.target.checked;
                                  return;  
                                }
                            
                        this.data[key]=click.target.value;
                    
                    }.bind(this))
                    }
                }
                });
            }
        }
        //binding declared Methods to events
        const events = ['click']
        const DOMelemnts = events.map(event =>{
            return {
                event:event,
                targets: this.container.querySelectorAll()
            };
            });
            for (const method in this.methods){
                if(this.methods.hasOwnProperty(method)){
                    this.methods[method]= this.methods[method].bind(this);
                }
            }
            DOMelemnts.forEach(element=> {
                element.targets.forEach(target => {
                    target.addEventListener(element.event,
                    this.methods[target.getAttribute('data-event'+ element.event)]);
                });
            });
        
    }
}
}