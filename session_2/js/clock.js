class Clock extends Component{
    constructor(){
        super({
            selector:'[data-component="clock"]',
            template:`
            <article class="reloj">
            <section class="display">
                <span class="hours" data-value="hours"></span>:
                <span class="minutes" data-value="minutes"></span>:
                <span class="seconds" data-value="seconds"></span>
            </section>
            <section class="controls">
                <button data-event-click="start">Start</button>
                <button data-event-click="pause">Pause</button>
                <button data-event-click="stop">Stop</button>
            </section>
            </article>`,
            data:{
                seconds:0,
                minutes: 0,
                hours: 0
            },
            methods:{
                updateClock(){
                    if(this.data.seconds==60)
                    {
                        this.data.seconds=0;
                        this.data.minutes ++;
                    }
                    if(this.data.minutes==60)
                    {
                        this.data.minutes=0;
                        this.data.hours ++;
                    }
                   
                },
                start(){
                if(!this.data.clockId)
                {
                    this.data.clockId= setInterval(function(){
                        this.data.seconds ++;
                        this.methods.updateClock();
                      
                           
                    }.bind(this),1000)
                }
                },
                pauseClock(){
                    clearInterval(this.data.clockId)
                    this.data.clockId=0;
                },
                stopClock(){
                    clearInterval(this.data.clockId)
                    this.data.seconds =0;
                    this.data.minutes =0;
                    this.data.hours =0;
                    this.data.clockId=0;
                    
                }
            }
        });
    }
}